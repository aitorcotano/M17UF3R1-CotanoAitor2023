using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public GameObject Player;
    public PlayableDirector Director;
    public bool isDead = false;
    public bool wonGame = false;

    public InventorySO inventory;
    [SerializeField]
    private GameObject[] blackObjImages;
    [SerializeField]
    private GameObject[] ObjImages;

    private AudioSource audioSource;
    [SerializeField]
    private AudioClip grabObjectSound;
    [SerializeField]
    private AudioClip errorSound;
    [SerializeField]
    private AudioClip successSound;
    


    public static GameManager Instance
    {
        get
        {
           
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        audioSource = GetComponent<AudioSource>();
        inventory.clearAll();
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public void PlayerDeath()
    {
        isDead = true;
        _instance.Player.GetComponent<CharacterMovement>().killPlayer();
    }

    public void GoToEndScreen()
    {
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("ResultScreen");
    }


    public void PlayGrabSound() { audioSource.PlayOneShot(grabObjectSound); }

    public void PlayErrorSound() { audioSource.PlayOneShot(errorSound); }

    public void PlaySuccessSound() { audioSource.PlayOneShot(successSound); }

    public void GrabObject(Objects type)
    {
        switch (type)
        {
            case Objects.Sword:
                inventory.hasSword = true;
                blackObjImages[0].SetActive(false);
                ObjImages[0].SetActive(true);
                break;
            case Objects.Shield:
                inventory.hasShield = true;
                blackObjImages[1].SetActive(false);
                ObjImages[1].SetActive(true);
                break;
            case Objects.Gun:
                inventory.hasGun = true;
                blackObjImages[2].SetActive(false);
                ObjImages[2].SetActive(true);
                break;
            case Objects.Key:
                inventory.hasKey = true;
                blackObjImages[3].SetActive(false);
                ObjImages[3].SetActive(true);
                break;
            default:
                break;
        }
    }
}
public enum Objects
{
    Sword,
    Shield,
    Gun,
    Key
}

