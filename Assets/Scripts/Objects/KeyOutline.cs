using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyOutline : GrabbableScript
{
    public override void OnGrab()
    {
        base.OnGrab();
        GameManager.Instance.GrabObject(Objects.Key);
    }
}
