using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 1)]
public class InventorySO : ScriptableObject
{
    public bool hasSword = false;
    public bool hasShield = false;
    public bool hasGun = false;
    public bool hasKey = false;

    public void clearAll()
    {
        hasSword = false;
        hasShield = false;
        hasGun = false;
        hasKey = false;
    }
}
