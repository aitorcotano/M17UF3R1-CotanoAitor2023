using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableScript : MonoBehaviour, IGrabbable
{
    [SerializeField]
    private Color outlineColor;


    private void Start()
    {
        var outline = gameObject.AddComponent<Outline>();
        outline.OutlineMode = Outline.Mode.OutlineVisible;
        outline.OutlineColor = outlineColor;
        outline.OutlineWidth = 3f;
    }

    public virtual void OnGrab()
    {
        GameManager.Instance.PlayGrabSound();
    }
}
