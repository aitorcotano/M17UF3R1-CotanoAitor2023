using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectUIResult : MonoBehaviour
{
    public GameObject[] resultImg;
    public GameObject[] blankImg;

    // Start is called before the first frame update
    void Start()
    {
        if(GameManager.Instance.inventory.hasSword == true)
        {
            resultImg[0].SetActive(true);
            blankImg[0].SetActive(false);
        }
        if (GameManager.Instance.inventory.hasShield == true)
        {
            resultImg[1].SetActive(true);
            blankImg[1].SetActive(false);
        }
        if (GameManager.Instance.inventory.hasGun == true)
        {
            resultImg[2].SetActive(true);
            blankImg[2].SetActive(false);
        }
        if (GameManager.Instance.inventory.hasKey == true)
        {
            resultImg[3].SetActive(true);
            blankImg[3].SetActive(false);
        }
    }
}
