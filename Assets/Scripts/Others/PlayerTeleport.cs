using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleport : MonoBehaviour
{
    CharacterMovement cm;

    // Start is called before the first frame update
    void Start()
    {
        cm = gameObject.GetComponent<CharacterMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("TP1"))
        {
            StartCoroutine(TeleportToInterior());
        }
        if(other.CompareTag("TP2"))
        {
            StartCoroutine(TeleportToExterior());
        }
    }

    IEnumerator TeleportToInterior()
    {
        cm.disabled = true;
        yield return new WaitForSeconds(0.25f);
        gameObject.transform.position = new Vector3(444.8f , -21f , 864.2f);
        yield return new WaitForSeconds(0.25f);
        cm.disabled = false;
    }

    IEnumerator TeleportToExterior()
    {
        cm.disabled = true;
        yield return new WaitForSeconds(0.25f);
        gameObject.transform.position = new Vector3(417.7f, 8.9f, 906f);
        yield return new WaitForSeconds(0.25f);
        cm.disabled = false;
    }
}
