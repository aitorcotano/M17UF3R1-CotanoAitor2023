using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarmfulProps : MonoBehaviour, IDamageable
{
    public void OnDamage()
    {
        GameManager.Instance.PlayerDeath();
    }
}
