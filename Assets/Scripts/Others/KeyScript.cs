using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    [SerializeField]
    private GameObject portal;
    private bool isOpen = false;
    [SerializeField]
    private GameObject message;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || isOpen) { return; }
            

        if (GameManager.Instance.inventory.hasKey == true)
        {
            portal.SetActive(true);
            isOpen = true;
            GameManager.Instance.PlaySuccessSound();
            GameManager.Instance.wonGame = true;
            StartCoroutine(EndGame());  
        }
        else
        {
            message.SetActive(true);
            GameManager.Instance.PlayErrorSound();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) { message.SetActive(false); }
    }

    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2f);
        GameManager.Instance.GoToEndScreen();
    }
}