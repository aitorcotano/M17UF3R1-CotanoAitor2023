using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    public float flickerIntensity;
    public float flickPerSecond;
    public float speed;

    private float _time;
    private float _startingIntensity;
    private Light _light;

    // Start is called before the first frame update
    void Start()
    {
        _light = GetComponent<Light>();
        _startingIntensity = _light.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime * (1 - Random.Range(-speed, speed)) * Mathf.PI;
        _light.intensity = _startingIntensity + Mathf.Sin(_time * flickPerSecond) * flickerIntensity;
    }
}
