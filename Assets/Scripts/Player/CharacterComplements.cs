using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterComplements : GrabbableScript
{
    [SerializeField]
    private GameObject complement;
    [SerializeField]
    private Objects objectType;

    public override void OnGrab()
    {
        base.OnGrab();
        complement.SetActive(true);
        GameManager.Instance.GrabObject(objectType);
    }
}
