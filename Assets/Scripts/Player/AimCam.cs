using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AimCam : MonoBehaviour
{
    private PlayerInputActions inputActions;
    private CinemachineVirtualCamera virtualCamera;
    [SerializeField]
    private GameObject AimCrosshair;
    [SerializeField]
    private GameObject NormalCrosshair;
    [SerializeField]
    private int priorityBoostAmount;

    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        inputActions = new PlayerInputActions();

        inputActions.Player.Aim.started += _ => startAim();
        inputActions.Player.Aim.canceled += _ => cancelAim();
    }

    private void OnEnable()
    {
        inputActions.Player.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player.Disable();
    }

    private void startAim()
    {
        virtualCamera.Priority += priorityBoostAmount;
        AimCrosshair.SetActive(true);
        NormalCrosshair.SetActive(false);
    }

    private void cancelAim()
    {
        virtualCamera.Priority -= priorityBoostAmount;
        AimCrosshair.SetActive(false);
        NormalCrosshair.SetActive(true);
    }
}
