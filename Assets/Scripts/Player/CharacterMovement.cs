using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Playables;

public class CharacterMovement : MonoBehaviour
{
    private Animator animator;
    public Vector3 anim_high;
    private float transTime;
    private PlayerInputActions inputActions;
    private CharacterController characterController;
    private Transform cameraTransform;

    [SerializeField] PlayableDirector pd;
    [SerializeField] GameObject timeLine;
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject weapon;
    [SerializeField] GameObject noweapon;

    [SerializeField]
    private Vector2 rawInput;
    [SerializeField]
    private Vector2 smoothedInput = Vector2.zero;
    [SerializeField]
    Vector3 rotatedInput;
    [SerializeField]
    private Vector3 walkInput;
    [SerializeField]
    private Vector3 runInput;
    private Vector2 inputSmoothVelocity;
    [SerializeField]
    private float inputSmoothSpeed;
    [SerializeField]
    private bool isMoving;
    [SerializeField]
    private bool isRunning;
    [SerializeField]
    private bool isCrouching;
    [SerializeField]
    private bool isJumping;
    [SerializeField]
    private bool isAiming;
    [SerializeField]
    private bool isGrabing;
    [SerializeField]
    private bool interactionBlocked;
    [SerializeField]
    public bool disabled = false;
    [SerializeField]
    private bool isShooting;
    [SerializeField]
    private float gravity;
    [SerializeField]
    private float jumpHeight;
    [SerializeField]
    private float walkSpeed;
    [SerializeField]
    private float runSpeed;
    [SerializeField]
    private float rotationSpeed;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        animator.gameObject.GetComponent<CharacterController>().center = new Vector3(anim_high.x, 0.2f, anim_high.z);
        transTime = animator.GetAnimatorTransitionInfo(0).duration;
        characterController = GetComponent<CharacterController>();
        inputActions = new PlayerInputActions();
        cameraTransform = Camera.main.transform;
        Cursor.lockState = CursorLockMode.Locked;

        inputActions.Player.Move.started += onMovementInput;
        inputActions.Player.Move.performed += onMovementInput;
        inputActions.Player.Move.canceled += onMovementInput;

        inputActions.Player.Run.started += onRun;
        inputActions.Player.Run.canceled += onRun;
        inputActions.Player.Crouch.started += onCrouch;
        inputActions.Player.Jump.started += onJump;

        inputActions.Player.Aim.started += onAim;
        inputActions.Player.Aim.canceled += onAim;
        inputActions.Player.Shoot.started += onShoot;

        inputActions.Player.Interact.started += onInteract;
        inputActions.Player.Dance.started += onDance;
        
    }

    private void OnEnable() { inputActions.Player.Enable(); }

    private void OnDisable() { inputActions.Player.Disable(); }

    private void Update()
    {
        if(!disabled)
        {
            handleAnimation();

            if (!interactionBlocked)
            {
                handleGravity();
                smoothMovement();
                handleRotation();
                handleJump();
                handleInteraction();
                handleMove();
                handleShooting();
            }
        }
    }

    private void onMovementInput(InputAction.CallbackContext context)
    {
        rawInput = context.ReadValue<Vector2>();
        isMoving = rawInput.magnitude != 0;
    }
    private void onRun(InputAction.CallbackContext context) => isRunning = context.ReadValueAsButton();
    private void onCrouch(InputAction.CallbackContext context) => isCrouching = !isCrouching;
    private void onJump(InputAction.CallbackContext context) => isJumping = characterController.isGrounded;
    private void onAim(InputAction.CallbackContext context) => isAiming = context.ReadValueAsButton();
    private void onInteract(InputAction.CallbackContext context) => isGrabing = context.ReadValueAsButton();
    private void onDance(InputAction.CallbackContext context) => interactionBlocked = context.ReadValueAsButton();
    private void onShoot(InputAction.CallbackContext context) => isShooting = context.ReadValueAsButton();

    private void handleMove()
    {
        if (isRunning && rawInput.y >= 0)
        {
            characterController.Move(runInput * Time.deltaTime);
            isCrouching = false;
        }
        else 
        {
            characterController.Move(walkInput * Time.deltaTime);
        } 
    }

    private void handleRotation()
    {
        if (isMoving || isAiming)
        {
            Quaternion targetRotation = Quaternion.Euler(0, cameraTransform.eulerAngles.y, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void handleAnimation()
    {
        bool isCrouched = animator.GetBool("Crouched");
        bool isJump = animator.GetBool("Jumping");
        bool isAimed = animator.GetBool("Aiming");
        bool isGrabbing = animator.GetBool("Grabbing");
        bool isShoot = animator.GetBool("Shooting");

        Vector3 localVelocity = characterController.velocity.x * transform.right * -1 + characterController.velocity.z * transform.forward;
        animator.SetFloat("Speed x", localVelocity.x);
        animator.SetFloat("Speed z", localVelocity.z);

        if (!isCrouched && isCrouching)
        {
            animator.SetBool("Crouched", true);
        }
        else if (isCrouched && !isCrouching)
        {
            animator.SetBool("Crouched", false);
        }    

        if (!isJump && isJumping)
        {
            animator.SetBool("Jumping", true);
        } 
        else if (isJump && !isJumping && characterController.isGrounded)
        {
            animator.SetBool("Jumping", false);
        }

        if (isAiming && !isAimed && !interactionBlocked && GameManager.Instance.inventory.hasGun == true)
        {
            animator.SetBool("Aiming", true);
            weapon.SetActive(true);
            noweapon.SetActive(false);
        } 
        else if (isAimed && !isAiming)
        {
            animator.SetBool("Aiming", false);
            weapon.SetActive(false);
            noweapon.SetActive(true);
        } 

        if (!isGrabbing && isGrabing && characterController.isGrounded && !interactionBlocked)
        {
            animator.SetBool("Grabbing", true);
        }
        else if (isGrabbing && !isGrabing)
        {
            animator.SetBool("Grabbing", false);
        }    

        if (interactionBlocked && !GameManager.Instance.isDead && GameManager.Instance.Director.state != UnityEngine.Playables.PlayState.Playing) { GameManager.Instance.Director.Play(); }

        if (!isShoot && isShooting && GameManager.Instance.inventory.hasGun == true)
        {
            animator.SetBool("Shooting", true);
        } 
        else if (isShoot && !isShooting)
        {
            animator.SetBool("Shooting", false);
        }

        if (Input.GetKeyDown(KeyCode.X)) 
        { 
            pd.Stop();
            timeLine.SetActive(false);
            canvas.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            timeLine.SetActive(true);
        }
    }
    
    public void handleHigh(float sign)
    {
        StartCoroutine(AnimHighFixer(animator, transTime, sign));
    }

    private void handleGravity()
    {
        if (characterController.isGrounded)
        {
            walkInput.y = -1f;
            runInput.y = -1f;
        }
        else
        {
            walkInput.y += gravity * Time.deltaTime;
            runInput.y += gravity * Time.deltaTime;
        }
    }

    private void handleJump()
    {
        if (isJumping && !isCrouching)
        {
            walkInput.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
            runInput.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
        }
        isJumping = false;
    }

    private void handleInteraction()
    {
        if (isGrabing) { isGrabing = false; }
    }

    private void handleShooting()
    {
        if (isShooting || !isAiming) { isShooting = false; }
    }

    public void freeInteraction()
    {
        interactionBlocked = false;
    }

    public void blockInteraction()
    {
        interactionBlocked = true;
    }

    public void killPlayer()
    {
        animator.SetTrigger("Dead");
        interactionBlocked = true;
        StartCoroutine(Death());
    }

    private void smoothMovement()
    {
        smoothedInput = Vector2.SmoothDamp(smoothedInput, rawInput, ref inputSmoothVelocity, inputSmoothSpeed);
        if (smoothedInput.magnitude < 0.01f) { smoothedInput = Vector2.zero; }
        rotatedInput = smoothedInput.x * cameraTransform.right + smoothedInput.y * cameraTransform.forward;
        Vector2 dir = new Vector2(rotatedInput.x, rotatedInput.z);
        if (isMoving) { dir.Normalize(); }
        rotatedInput = new Vector3(dir.x, 0, dir.y);
        walkInput = rotatedInput * walkSpeed + walkInput.y * Vector3.up;
        runInput = rotatedInput * runSpeed + runInput.y * Vector3.up;
    }

    IEnumerator AnimHighFixer(Animator animator, float duration, float sign)
    {
        int N = 75;
        float dif = 0.35f;
        for (int i = 0; i < N; i++)
        {
            GetComponent<CharacterController>().center += new Vector3(0,(dif/(N)) * sign, 0);
            yield return new WaitForSeconds(transTime/N);
        }
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(4f);
        GameManager.Instance.GoToEndScreen();
    }
}
