using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class NavScript : MonoBehaviour
{
    private PlayerInputActions inputActions;
    private NavMeshAgent agent;
    private Animator animator;

    [SerializeField]
    private States state;
    [SerializeField]
    private bool followingPlayer = true;

    [SerializeField]
    private Transform point;
    [SerializeField]
    private Transform player;
    public float chaseRange = 20f;
    private Vector3 currentDestination;

    private void Awake()
    {
        inputActions = new PlayerInputActions();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        inputActions.Player.AgentDestination.started += onAgentDest;
    }

    private void OnEnable() { inputActions.Player.Enable(); }

    private void OnDisable() { inputActions.Player.Disable(); }

    private void onAgentDest(InputAction.CallbackContext context) => followingPlayer = !followingPlayer;

    // Start is called before the first frame update
    void Start()
    {
        currentDestination = point.position;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.transform.position, gameObject.transform.position);
        
        if (distance <= chaseRange)
        {
            followingPlayer = true;
            currentDestination = player.position;
        }
        else
        {
            currentDestination = point.position;
            followingPlayer = false;
        }
            
        agent.SetDestination(currentDestination);

        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            animator.SetBool("Running", false);
            if (followingPlayer/*agent.destination == player.position*/)
            {
                state = States.Attacking;
            }  
            else
            {
                state = States.Idle;
            }         
        }
        else
        {
            state = States.Walking;
            animator.SetBool("Running", true);
        }
            
        handleAnimation();
    }

    void handleAnimation()
    {
        bool isAttacking = animator.GetBool("Attacking");

        if (!isAttacking && state == States.Attacking)
        {
            animator.SetBool("Attacking", true);
        }  
        else if (isAttacking && state != States.Attacking)
        {
            animator.SetBool("Attacking", false);
        }
          
    }
}

enum States
{
    Idle,
    Walking,
    Attacking
}
